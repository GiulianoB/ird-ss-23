const express = require("express");
const pgp = require("pg-promise")();
const server = express();
const port = 4005;

const database = pgp({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "postgres",
  database: "postgres",
});

server.use(express.json());

server.use('/', express.static('public'));

server.get("/hallo-world", (req, res) => {
  res.send("Hallo bei der SW App!");
});

server.get("/sights", (req, res) => {
  let sqlQuery = "select * from sights"
  if (req.query.format && req.query.format == 'geojson') {
    sqlQuery = "select json_build_object('type', 'FeatureCollection', 'features', json_agg(ST_AsGeoJSON(s.*)::json)) as geojson from sights s";
  }
  if (req.query.format && req.query.format == 'gml') {
    sqlQuery = "select ST_ASGml(geometry) from sights"
  }

  if (req.query) {
    sqlQuery += " where true";
    if (req.query.type) sqlQuery += ` and type = ${req.query.type}`;
    if (req.query.maxPrice) sqlQuery += ` and price <= ${req.query.maxPrice}`;
  }

  database
    .any(sqlQuery)
    .then(function (data) {
      if (req.query.format && req.query.format === 'gml') {
        let gmlString = ""
        for (i = 0; i < data.length; i++) {
          gmlString += data[i].st_asgml
        }
        res.status(200).send(gmlString);
      } else {
        res.status(200).json(data);
      }
    })
    .catch(function (error) {
      console.log(error);
      res.status(500).json({ message: "Something went wrong!" });
    });
});

server.post("/sights", (req, res) => {
  // Prüfe ob vom Client alles richtig gemacht wurde
  // Falls nicht gib einen 400 (Bad Request) Fehler zurück
  if (
    !req.body.name ||
    !req.body.type ||
    !req.body.price ||
    !req.body.lat ||
    !req.body.lng
  ) {
    res
      .status(400)
      .json({
        message: "Attribute name, type, price, lat und lng sind erforderlich",
      });
    return;
  }

  // Schreibe die vom Client kommenden Daten in die Datenbak
  database
    .one(
      "insert into sights (name, type, price, note, geometry) values ($1, $2, $3, $4, ST_MakePoint($5, $6)) returning *;",
      [
        req.body.name,
        req.body.type,
        req.body.price,
        req.body.note,
        req.body.lat,
        req.body.lng,
      ]
    )
    // Falls alles geklappt hat, gib den neu erstellten Datensatz plus Status Code 201 an den Client zurück
    .then(function (data) {
      res.status(201).json(data);
    })
    // Falls es einen Fehler gab, gib eine Fehlermeldung plus Status Code 500 zurück
    .catch(function () {
      res.status(500).json({ message: "Something went wrong!" });
    });
});

server.delete("/sights/:id", (req, res) => {
  // Beispiel: So könnte man prüfen ob der URL Parameter "id" in den richtigen Datentyp konvertierbar ist
  if (isNaN(parseInt(req.params.id))) {
    res.status(400).json({ message: "Url param id must be a number" });
    return;
  }

  database
    .none("delete from sights where id = $1", [req.params.id])
    .then(function () {
      res.status(204).json();
    })
    .catch(function () {
      res.status(500).json({ message: "Something went wrong!" });
    });
});

server.patch("/sights/:id", (req, res) => {
  let sqlQuery = "update sights set";
  if (req.body.name) sqlQuery += ` name = '${req.body.name}',`;
  if (req.body.type) sqlQuery += ` type = '${req.body.type}',`;
  if (req.body.price) sqlQuery += ` price = ${req.body.price},`;
  if (req.body.note) sqlQuery += ` note = '${req.body.note}',`;
  if (req.body.lat) sqlQuery += ` lat = ${req.body.lat},`;
  if (req.body.lng) sqlQuery += ` lng = ${req.body.lng},`;
  sqlQuery = sqlQuery.substring(0, sqlQuery.length - 1);
  sqlQuery += `where id = ${req.params.id} returning *;`;

  database
    .one(sqlQuery)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (error) {
      res.status(500).json({ message: "Something went wrong!", error: error });
    });
});

server.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
