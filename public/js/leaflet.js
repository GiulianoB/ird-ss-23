// Leaflet initialisierung
var map = L.map("map").setView([48.7758, 9.1829], 10);
L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
  maxZoom: 19,
  attribution:
    '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
}).addTo(map);

// Use Leaflet Geocoder
L.Control.geocoder().addTo(map);

// Customize Leaflet Geocoder
// var geocoder = L.Control.geocoder({
//   defaultMarkGeocode: false
// })
//   .on('markgeocode', function(e) {
//     var bbox = e.geocode.bbox;
//     var poly = L.polygon([
//       bbox.getSouthEast(),
//       bbox.getNorthEast(),
//       bbox.getNorthWest(),
//       bbox.getSouthWest()
//     ]).addTo(map);
//     map.fitBounds(poly.getBounds());
//   })
//   .addTo(map);

function onMapClick(e) {
  // is bound to the map object later
  console.log("Es wurde an die Stelle " + e.latlng + " geklickt ");
  var lat = e.latlng.lat;
  var lng = e.latlng.lng;
  $("#latInput").val(lat);
  $("#lngInput").val(lng);
  draggableMarker = L.marker([lat, lng], {
    draggable: true,
    zIndexOffset: 900,
  }).addTo(map);
  draggableMarker.on("dragend", function (e) {
    $("#latInput").val(lat);
    $("#lngInput").val(lng);
  });
}

map.on('click ', onMapClick);