// Get sights
function getSights() {
  $.ajax({
    url: "http://localhost:4005/sights",
    type: "GET",
    data: { format: 'geojson'},
  })
    .done(function (data) {
      L.geoJSON(data[0].geojson).addTo(map);
      createSightsList(data[0].geojson);
    })
    .fail(function () {
      console.log("failed to get data");
    })
    .always(function () {
      console.log("done");
    });
}
$("#getSightsButton").on("click", function () {
  getSights();
});

// Post sight
$("#postSight").on("click", function () {
  // console.log($('#nameInput').val());
  let jsonBody = JSON.stringify({
    name: $("#nameInput").val(),
    type: $("#typeInput").val(),
    price: $("#priceInput").val(),
    note: $("#noteInput").val(),
    lat: $("#latInput").val(),
    lng: $("#lngInput").val(),
  });
  $.ajax({
    url: "http://localhost:4005/sights",
    type: "POST",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    data: jsonBody,
  })
    .done(function (data) {
      console.log(data);
    })
    .fail(function () {
      console.log("failed to post sight");
    })
    .always(function () {
      console.log("done");
    });
});

function createSightsList(geojson) {
  $("#sightsList").html("");
  for (let i = 0; i < geojson.features.length; i++) {
    console.log(geojson.features[i].properties.name);
    $("#sightsList").prepend(`
            <li>
                ${geojson.features[i].properties.name} | ${geojson.features[i].properties.id}
                <button onclick="deleteSight(${geojson.features[i].properties.id})" >Delete</button>
            </li>
        `);
  }
}

// Delete sight
function deleteSight(id) {
  console.log(id);
  $.ajax({
    url: `http://localhost:4005/sights/${id}`,
    type: "DELETE",
    data: null,
  })
    .done(function () {
      console.log("sight deleted");
      getSights();
    })
    .fail(function () {
      console.log("failed to delete sight");
    })
    .always(function () {
      console.log("done");
    });
}
