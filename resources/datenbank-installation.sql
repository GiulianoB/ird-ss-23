-- Install PostGIS
create extension if not exists postgis

-- Sight Tabell
drop table if exists sights;
create table sights (
	id serial primary key,
	name text not null,
	type text not null,
	price numeric not null,
	note text null,
	geometry geometry
);

insert into sights (name, type, price, note, geometry)
values ('Fernsehturm', 'others', 7, 'Meine erste Sehenswürdigkeit', ST_MakePoint(9.1829, 48.7758));

insert into sights (name, type, price, note, geometry)
values ('Schloss Neuschwanstein', 'history', 7, 'Meine erste Sehenswürdigkeit', ST_MakePoint(9.2829, 48.8758));

insert into sights (name, type, price, note, geometry)
values ('Deutsches Museum', 'history', 7, 'Meine erste Sehenswürdigkeit', ST_MakePoint(9.2729, 48.5758));

insert into sights (name, type, price, note, geometry)
values ('Mercedes-Benz Museum', 'history', 7, 'Meine erste Sehenswürdigkeit', ST_MakePoint(9.1229, 48.7158));


-- Beispiel GIS Abfrage: Selektiere alle geometrien die in einem Radius um einen Punkt liegen
-- select * from sights
-- where ST_DWithin(geometry, ST_MakePoint(9.1829, 48.7758)::geography, 50000);

